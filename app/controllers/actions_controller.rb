class ActionsController < ApplicationController
  require 'line/bot'  # gem 'line-bot-api'
  require 'open-uri'
  require 'kconv'
  require 'rexml/document'

  protect_from_forgery :except => [:callback]

  def callback
    # 要検証
    body = request.body.read
    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature)
      error 400 do 'Bad Request' end
    end
    events = client.parse_events_from(body)
    events.each do |event|
      case event
        when Line::Bot::Event::Message # メッセージが送信された場合の対応（機能①）
          reply_text
          message = { type: 'text', text: push }
          client.reply_message(event['replyToken'], message)
        when Line::Bot::Event::Follow # LINEお友達追された場合（機能②）
          line_id = event['source']['userId'] # 登録したユーザーのidをユーザーテーブルに格納
          User.create(line_id: line_id)
        when Line::Bot::Event::Unfollow # LINEお友達解除された場合（機能③）
          line_id = event['source']['userId']
          User.find_by(line_id: line_id).destroy # お友達解除したユーザーのデータをユーザーテーブルから削除
      end
    end
    head :ok
  end

  private

    def client
      @client ||= Line::Bot::Client.new do |config|
        config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
        config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
      end
    end

    def reply_text
      case event.type
        when Line::Bot::Event::MessageType::Text # ユーザーからテキスト形式のメッセージが送られて来た場合
          input = event.message['text'] # event.message['text']：ユーザーから送られたメッセージ
          url  = "https://www.drk7.jp/weather/xml/13.xml"
          xml  = open( url ).read.toutf8
          doc = REXML::Document.new(xml)
          xpath = 'weatherforecast/pref/area[4]/'
          min_per = 30 # 当日朝のメッセージの送信の下限値は20％としているが、明日・明後日雨が降るかどうかの下限値は30％としている
          case input
            when /.*(明日|あした).*/
              rainfall_message(2)
            when /.*(明後日|あさって).*/
              rainfall_message(3)
          end
      else # テキスト以外（画像等）のメッセージが送られた場合
        push = "テキストで書いてください"
      end
    end

    def rainfall_message(day)
      day_table = {2 => '明日', 3 => '明後日'}
      time_table = {2 => '', 3 => '', 4 => ''}
      rainny = false
      (2..4).each do |num|
         time_table[num] = doc.elements[xpath + "info[#{day}]/rainfallchance/period[#{num}]"].text
         rainny = true if time_table[num].to_i >= min_per
      end
      message(rainny, day_table[day])
    end

    def messsage(rainny, day)
      if rainny == true && day == "明日"
        push = "明日は雨\n降水確率は\n6〜12時#{time_table[2]}％\n12〜18時#{tame_table[3]}％\n18〜24時#{time_table[4]}％"
      elsif rainny == false && day == "明日"
        push = "明日は晴れ！！"
      elsif rainny == true && day == "明後日"
        push = "明後日は雨っぽい"
      elsif rainny == false && day == "明後日"
        push = "明後日は晴れっぽい"
      else
        push = "わかんない"
      end
    end
end
